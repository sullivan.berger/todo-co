## How to contribute

[Fork the repository](https://gitlab.com/sullivan.berger/todo-co/-/forks/new)

## Setup

1. Clone the repository  
   ``git clone https://gitlab.com/sullivan.berger/todo-co.git``


2. Configure your database in .env  
   ``DATABASE_URL="postgresql://username:password@host:5432/database?serverVersion=13&charset=utf8"``


3. Install dependencies  
   ``composer install``

4. Create database  
   ``bin/console doctrine:database:create``

5. Execute migrations  
   `` bin/console doctrine:migrations:migrate``

6. Load fixtures  
   ``bin/console doctrine:fixtures:load``

7. Execute tests  
   ``bin/phpunit``

## Propose your work

**Please keep in mind that your work must comply with PSR-12.**

1. Create your own branch  
``git checkout -b your-branch-name``

2. Update the code to your needs, and then add the files with command  
   ``git add [your_files]``

3. Write tests and check if all tests are successful with command  
   ``bin/phpunit``

5. Commit your changes and specify what you worked on with an explicit message  
   ``git commit -m "[your_message]``

6. Push your work on the newly created branch  
``git push origin your-branch-name``

7. [Open a merge request on the project repository](https://gitlab.com/sullivan.berger/todo-co/-/merge_requests/new)

