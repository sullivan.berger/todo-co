# ToDoList

---

OpenClassrooms - P8 - Improve an existing project  
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/41f0a144491d489a802444384bf99f22)](https://www.codacy.com/gl/sullivan.berger/todo-co/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=sullivan.berger/todo-co&amp;utm_campaign=Badge_Grade)

## Pre-requisites

- PHP > 8.1
- Postgres > 13.3
