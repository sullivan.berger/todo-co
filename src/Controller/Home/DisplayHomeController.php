<?php

namespace App\Controller\Home;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DisplayHomeController extends AbstractController
{
    #[Route("/", "home")]
    public function __invoke(): Response
    {
        return $this->render("home/index.html.twig");
    }
}
