<?php

namespace App\Controller\Task;

use App\Entity\Task;
use App\Form\TaskType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class CreateTaskController extends AbstractController
{
    #[Route("/tasks/create", "tasks_create")]
    public function __invoke(
        Request $request,
        EntityManagerInterface $entityManager,
        FormFactoryInterface $formFactory,
        TranslatorInterface $translator
    ): RedirectResponse|Response
    {
        if (!$this->getUser()) {
            $this->addFlash("error", $translator->trans("redirect.user.not_connected"));
            return $this->redirectToRoute("home");
        }

        $form = $formFactory->create(TaskType::class);
        $form->handleRequest($request);

        $task = new Task();

        if ($form->isSubmitted() && $form->isValid()) {
            $task
                ->setTitle($form->get("title")->getData())
                ->setContent($form->get("content")->getData())
                ->setAuthor($this->getUser())
                ;

            $entityManager->persist($task);
            $entityManager->flush();

            return $this->redirectToRoute("home");
        }

        return $this->render("task/create.html.twig", [
            "form" => $form->createView()
        ]);
    }
}
