<?php

namespace App\Controller\Task;

use App\Entity\Task;
use App\Form\DeleteTaskType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class DeleteTaskByIdController extends AbstractController
{
    #[Route("/tasks/{id}/delete", "tasks_delete_by_id")]
    #[ParamConverter("task", Task::class)]
    public function __invoke(
        Task $task,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        FormFactoryInterface $formFactory,
        Request $request
    ): RedirectResponse|Response
    {
        if (!$this->isGranted("delete", $task)) {
            $this->addFlash("error", $translator->trans("redirect.tasks.access_denied"));
            return $this->redirectToRoute(('home'));
        }

        $form = $formFactory->create(DeleteTaskType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->remove($task);
            $entityManager->flush();

            $this->addFlash("success", $translator->trans("redirect.tasks.delete_successful"));
            return $this->redirectToRoute("home");
        }

        return $this->render("task/delete.html.twig", [
            "task" => $task,
            "form" => $form->createView()
        ]);
    }
}
