<?php

namespace App\Controller\Task;

use App\Repository\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class DisplayAllFinishedTasksController extends AbstractController
{
    #[Route("/tasks/done", "tasks_display_all_done")]
    public function __invoke(
        TaskRepository $taskRepository,
        TranslatorInterface $translator
    ): Response
    {
        if (!$this->getUser()) {
            $this->addFlash("error", $translator->trans("redirect.user.not_connected"));
            return $this->redirectToRoute("home");
        }
        return $this->render("task/done.html.twig", [
            "tasks" => $taskRepository->findBy(["author" => $this->getUser(), "isDone" => true])
        ]);
    }
}
