<?php

namespace App\Controller\Task;

use App\Repository\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class DisplayAllTasksController extends AbstractController
{
    #[Route("/tasks", "tasks_display_all")]
    public function __invoke(
        TaskRepository $taskRepository,
        TranslatorInterface $translator
    ): Response
    {
        if (!$this->getUser()) {
            $this->addFlash("error", $translator->trans("redirect.user.not_connected"));
            return $this->redirectToRoute("home");
        }

        if (in_array("ROLE_ADMIN", $this->getUser()->getRoles())) {
            $tasks = $taskRepository->findByRoleAdmin($this->getUser());
        } else {
            $tasks = $taskRepository->findBy(["author" => $this->getUser(), "isDone" => false]);
        }

        return $this->render("task/index.html.twig", [
            "tasks" => $tasks
        ]);
    }
}
