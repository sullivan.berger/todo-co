<?php

namespace App\Controller\Task;

use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class FinishTaskByIdController extends AbstractController
{
    #[Route("/tasks/{id}/finish", "task_toggle")]
    #[ParamConverter("task", Task::class)]
    public function __invoke(
        Task $task,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator
    ): RedirectResponse|Response
    {
        if (!$this->isGranted("update", $task)) {
            $this->addFlash("error", $translator->trans("redirect.tasks.access_denied"));
            return $this->redirectToRoute("home");
        }

        $task->toggle(!$task->isDone());

        $entityManager->flush();

        $this->addFlash("success", $translator->trans("redirect.tasks.done"));
        return $this->redirectToRoute("tasks_display_all_done");
    }
}
