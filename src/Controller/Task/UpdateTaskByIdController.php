<?php

namespace App\Controller\Task;

use App\Entity\Task;
use App\Form\TaskType;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class UpdateTaskByIdController extends AbstractController
{
    #[Route("/tasks/{id}/update", "tasks_update")]
    #[ParamConverter("task", Task::class)]
    public function __invoke(
        Task $task,
        EntityManagerInterface $entityManager,
        FormFactoryInterface $formFactory,
        Request $request,
        TranslatorInterface $translator
    ): Response
    {

        if (!$this->isGranted("update", $task)) {
            $this->addFlash("error", $translator->trans("redirect.tasks.access_denied"));
            return $this->redirectToRoute(('home'));
        }

        $form = $formFactory->create(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task
                ->setTitle($form->get("title")->getData())
                ->setContent($form->get("content")->getData())
                ;

            $entityManager->flush();

            return $this->redirectToRoute("home");
        }

        return $this->render("task/update.html.twig", [
            'task' => $task,
            'form' => $form->createView()
        ]);
    }
}
