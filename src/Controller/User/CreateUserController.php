<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class CreateUserController extends AbstractController
{
    #[Route("/users/create", "users_create")]
    public function __invoke(
        Request $request,
        EntityManagerInterface $entityManager,
        FormFactoryInterface $formFactory,
        UserPasswordHasherInterface $passwordHasher
    ): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute("home");
        }

        $form = $formFactory->create(UserType::class);
        $form->handleRequest($request);

        $user = new User();
        if ($form->isSubmitted() && $form->isValid()) {

            $user
                ->setUsername($form->get("username")->getData())
                ->setEmail($form->get("email")->getData())
                ->setPassword($passwordHasher->hashPassword($user, $form->get("password")->getData()))
                ;

            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute("home");
        }

        return $this->render("user/create.html.twig", [
            "form" => $form->createView()
        ]);
    }
}
