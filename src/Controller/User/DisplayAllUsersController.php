<?php

namespace App\Controller\User;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class DisplayAllUsersController extends AbstractController
{
    #[Route("/users", "users_display_all")]
    public function __invoke(
        UserRepository $userRepository,
        TranslatorInterface $translator
    ): Response
    {
        if (!$this->isGranted("ROLE_ADMIN")) {
            $this->addFlash("error", $translator->trans("redirect.user.access_denied"));
            return $this->redirectToRoute("home");
        }

        return $this->render("user/index.html.twig", [
            "users" => $userRepository->findAll()
        ]);
    }
}
