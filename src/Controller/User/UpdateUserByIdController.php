<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class UpdateUserByIdController extends AbstractController
{
    #[Route("/users/{id}/update", "users_update_by_id")]
    #[ParamConverter("user", User::class)]
    public function __invoke(
        User $user,
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        Request $request
    ): Response
    {
        if (!$this->isGranted("ROLE_ADMIN")) {
            $this->addFlash("error", $translator->trans("redirect.user.access_denied"));
            return $this->redirectToRoute("home");
        }

        $form = $formFactory->create(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user
                ->setRoles([$form->get("roles")->getData()])
                ;

            $entityManager->flush();

            $this->addFlash("success", $translator->trans("redirect.user.updated"));
            return $this->redirectToRoute("home");
        }

        return $this->render("user/update.html.twig", [
            "user" => $user,
            "form" => $form->createView()
        ]);
    }
}
