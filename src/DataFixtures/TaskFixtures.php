<?php

namespace App\DataFixtures;

use App\Factory\TaskFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TaskFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        TaskFactory::createMany(50);
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class
        ];
    }
}
