<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, ['label' => "form.username.label"])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'form.password.unmatch',
                'required' => true,
                'first_options'  => ['label' => 'form.password.label'],
                'second_options' => ['label' => 'form.password.confirm_label'],
            ])
            ->add('email', EmailType::class, ['label' => 'form.email.label'])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $formEvent) {
            $entity = $formEvent->getData();
            $form = $formEvent->getForm();

            if ($entity) {
                if (in_array("ROLE_ADMIN", $entity->getRoles())) {
                    $role = "ROLE_ADMIN";
                } else {
                    $role = "ROLE_USER";
                }

                $form->add('roles', ChoiceType::class, [
                    "label" => "form.role.label",
                    "choices" => [
                        "form.role.user" => "ROLE_USER",
                        "form.role.admin" => "ROLE_ADMIN"
                    ],
                    "mapped" => false,
                    "data" => $role
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                "data_class" => User::class
            ]);
    }
}
