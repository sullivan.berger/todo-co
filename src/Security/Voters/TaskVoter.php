<?php

namespace App\Security\Voters;

use App\Entity\Task;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class TaskVoter extends Voter
{
    const UPDATE = 'update';
    const DELETE = 'delete';
    private Security $security;

    public function __construct(
        Security $security
    )
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!in_array($attribute, [self::UPDATE, self::DELETE])) {
            return false;
        }

        if (!$subject instanceof Task) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        /** @var Task $task */
        $task = $subject;

        switch ($attribute) {
            case self::UPDATE:
                return $this->canUpdate($task, $user);
            case self::DELETE:
                return $this->canDelete($task, $user);
        }

        throw new \LogicException('This code should not be reached');
    }

    private function canDelete(Task $task, ?User $user): bool
    {
        if ($user === $task->getAuthor()) {
            return true;
        }

        if ($task->getAuthor() === null && $this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        return false;
    }

    private function canUpdate(Task $task, ?User $user): bool
    {
        if ($user === $task->getAuthor()) {
            return true;
        }

        return false;
    }
}
