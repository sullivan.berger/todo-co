<?php

namespace App\Tests\Application\Controller\Home;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DisplayHomeControllerTest extends WebTestCase
{
    public function testDisplayHomePageIsSuccessful(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
    }
}
