<?php

namespace App\Tests\Application\Controller\Task;

use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CreateTaskControllerTest extends WebTestCase
{
    public function testDisplayCreateTaskPageIsSuccessful(): void
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneBy([]);

        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/tasks/create');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h3', 'Créer une tâche');

    }

    public function testSubmitCreateTaskIsSuccessful()
    {
        $client = static::createClient();

        $taskRepository = static::getContainer()->get(TaskRepository::class);
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneBy([]);

        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/tasks/create');
        $this->assertResponseIsSuccessful();

        $title = sprintf("Title %s", random_int(1, 1000));
        $task = $taskRepository->findOneBy(["title" => $title]);
        $this->assertNull($task);

        $submitButton = $crawler->selectButton("Ajouter");
        $testForm = $submitButton->form([
            'task[title]' => $title,
            'task[content]' => "Test",
        ]);

        $client->submit($testForm);

        $this->assertResponseRedirects('/');
        $task = $taskRepository->findOneBy(["title" => $title]);
        $this->assertNotNull($task);
    }

    public function testDisplayCreateTaskPageRedirectsIfNotLoggedIn()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/tasks/create');

        $this->assertResponseRedirects('/');
    }
}
