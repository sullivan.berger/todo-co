<?php

namespace App\Tests\Application\Controller\Task;

use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DeleteTaskByIdControllerTest extends WebTestCase
{
    public function testDeleteTaskIsSuccessful(): void
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $taskRepository = static::getContainer()->get(TaskRepository::class);

        $testTask = $taskRepository->findOneBy([]);
        $testUser = $userRepository->findOneBy(["email" => $testTask->getAuthor()->getEmail()]);

        $client->loginUser($testUser);

        $crawler = $client->request('GET', sprintf("/tasks/%s/delete", $testTask->getId()));

        $this->assertResponseIsSuccessful();

        $submitButton = $crawler->selectButton("Confirmer la suppression");
        $testForm = $submitButton->form();

        $client->submit($testForm);

        $this->assertResponseRedirects('/', 302, "La tâche a été correctement supprimée.");
    }

    public function testDisplayDeleteTaskRedirectsIfNotLoggedIn()
    {
        $client = static::createClient();

        $taskRepository = static::getContainer()->get(TaskRepository::class);

        $testTask = $taskRepository->findOneBy([]);

        $crawler = $client->request('GET', sprintf("/tasks/%s/delete", $testTask->getId()));

        $this->assertResponseRedirects('/');
    }
}
