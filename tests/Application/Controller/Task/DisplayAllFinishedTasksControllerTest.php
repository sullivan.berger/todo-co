<?php

namespace App\Tests\Application\Controller\Task;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DisplayAllFinishedTasksControllerTest extends WebTestCase
{

    public function testDisplayAllFinishedTasksPageIsSuccessful()
    {
        $client = static::createClient();

        $userRepository = $client->getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneBy([]);

        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/tasks/done');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h3', 'Liste des tâches terminées');
    }
    public function testDisplayAllFinishedTasksPageRedirectsIfNotLoggedIn(): void
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/tasks/done');

        $this->assertResponseRedirects('/');
    }
}
