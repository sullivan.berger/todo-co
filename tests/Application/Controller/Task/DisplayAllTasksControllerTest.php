<?php

namespace App\Tests\Application\Controller\Task;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DisplayAllTasksControllerTest extends WebTestCase
{
    public function testDisplayAllTasksPageIfRoleAdminIsSuccessful(): void
    {
        $client = static::createClient();

        $userRepository = $client->getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByRoleAdmin();

        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/tasks');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h3', 'Liste des tâches');
    }

    public function testDisplayAllTasksPageIfRoleUserIsSuccessful(): void
    {
        $client = static::createClient();

        $userRepository = $client->getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByRoleUser();

        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/tasks');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h3', 'Liste des tâches');
    }

    public function testDisplayAllTasksPageRedirectsIfNotLoggedIn()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/tasks');

        $this->assertResponseRedirects('/');
    }
}
