<?php

namespace App\Tests\Application\Controller\Task;

use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FinishTaskByIdControllerTest extends WebTestCase
{
    public function testSubmitTaskIsDone()
    {
        $client = static::createClient();

        $taskRepository = static::getContainer()->get(TaskRepository::class);

        $testUser = $taskRepository->findOneBy(["isDone" => false])->getAuthor();
        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/tasks');
        $this->assertSelectorTextContains('h3', 'Liste des tâches');
        $this->assertResponseIsSuccessful();

        $submitButton = $crawler->selectButton("Marquer comme faite");
        $testForm = $submitButton->form();
        $client->submit($testForm);

        $this->assertResponseRedirects("/tasks/done");
    }

    public function testDisplayRedirectsIfInsufficientRights()
    {
        $client = static::createClient();

        $taskRepository = static::getContainer()->get(TaskRepository::class);
        $task = $taskRepository->findOneBy([]);

        $crawler = $client->request('GET', '/tasks');
        $this->assertResponseRedirects("/");
    }

    public function testDisplayUpdateTaskPageRedirectsIfNotLoggedIn()
    {
        $client = static::createClient();

        $taskRepository = static::getContainer()->get(TaskRepository::class);

        $testTask = $taskRepository->findOneBy([]);

        $crawler = $client->request('GET', sprintf("/tasks/%s/finish", $testTask->getId()));

        $this->assertResponseRedirects('/');
    }
}
