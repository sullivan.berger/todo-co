<?php

namespace App\Tests\Application\Controller\Task;

use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UpdateTaskByIdControllerTest extends WebTestCase
{
    public function testDisplayUpdateTaskPageIsSuccessful(): void
    {
        $client = static::createClient();

        $taskRepository = static::getContainer()->get(TaskRepository::class);
        $testTask = $taskRepository->findOneBy(["isDone" => false]);
        $testUser = $testTask->getAuthor();

        $client->loginUser($testUser);

        $crawler = $client->request('GET', sprintf("/tasks/%s/update", $testTask->getId()));

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h3', sprintf('Modifier la tâche %s', $testTask->getTitle()));
    }

    public function testSubmitUpdateTaskIsSuccessful()
    {
        $client = static::createClient();

        $taskRepository = static::getContainer()->get(TaskRepository::class);
        $testTask = $taskRepository->findOneBy(["isDone" => false]);
        $testUser = $testTask->getAuthor();

        $client->loginUser($testUser);

        $crawler = $client->request('GET', sprintf("/tasks/%s/update", $testTask->getId()));

        $title = sprintf("Title %s", random_int(1, 1000));

        $submitButton = $crawler->selectButton("Modifier");
        $testForm = $submitButton->form([
            'task[title]' => $title,
        ]);

        $client->submit($testForm);

        $this->assertResponseRedirects('/');
        $task = $taskRepository->findOneBy(["title" => $title]);
        $this->assertNotNull($task);
    }

    public function testDisplayUpdateTaskPageRedirectsIfNotLoggedIn()
    {
        $client = static::createClient();

        $taskRepository = static::getContainer()->get(TaskRepository::class);

        $testTask = $taskRepository->findOneBy([]);

        $crawler = $client->request('GET', sprintf("/tasks/%s/update", $testTask->getId()));

        $this->assertResponseRedirects('/');
    }
}
