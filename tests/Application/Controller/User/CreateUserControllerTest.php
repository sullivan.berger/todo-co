<?php

namespace App\Tests\Application\Controller\User;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CreateUserControllerTest extends WebTestCase
{
    public function testDisplayCreateUserPageIsSuccessful(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/users/create');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Créer un utilisateur');
    }

    public function testCreateUserPageRedirectsIfLoggedIn(): void {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);

        $testUser = $userRepository->findOneBy([]);

        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/users/create');
        $this->assertResponseRedirects('/');
    }

    public function testCreateUserFormSubmitIsSuccessful()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);

        $crawler = $client->request('GET', '/users/create');
        $this->assertResponseIsSuccessful();

        $username = sprintf("Toto%s", random_int(1, 1000));

        $user = $userRepository->findOneBy(["username" => $username]);
        $this->assertNull($user);

        $submitButton = $crawler->selectButton("Ajouter");
        $testForm = $submitButton->form([
            'user[username]' => $username,
            'user[email]' => sprintf("toto%s@toto.com", random_int(1,1000)),
            'user[password][first]' => 'Todo2022!',
            'user[password][second]' => 'Todo2022!',
        ]);

        $client->submit($testForm);

        $this->assertResponseRedirects('/');
        $user = $userRepository->findOneBy(["username" => $username]);
        $this->assertNotNull($user);
    }
}
