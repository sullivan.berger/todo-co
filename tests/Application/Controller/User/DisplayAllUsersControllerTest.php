<?php

namespace App\Tests\Application\Controller\User;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DisplayAllUsersControllerTest extends WebTestCase
{
    public function testDisplayAllUsersPageIsSuccessful(): void
    {
        $client = static::createClient();

        $userRepository = $client->getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByRoleAdmin();

        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/users');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Liste des utilisateurs');
    }

    public function testDisplayAllUsersPageRedirectsIfInsufficientRole()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/users');
        $this->assertResponseRedirects('/');
    }
}
