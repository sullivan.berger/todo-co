<?php

namespace App\Tests\Application\Controller\User;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UpdateUserByIdControllerTest extends WebTestCase
{
    public function testDisplayUserToUpdatePageIsSuccessful(): void
    {
        $client = static::createClient();

        $userRepository = $client->getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByRoleAdmin();
        $editedUser = $userRepository->findOneBy([]);

        $client->loginUser($testUser);

        $crawler = $client->request('GET', sprintf('/users/%s/update', $editedUser->getId()));

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', sprintf('Modifier %s', $editedUser->getUsername()));
    }

    public function testSubmitUpdateToRoleAdminFormIsSuccessful()
    {
        $client = static::createClient();

        $userRepository = $client->getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByRoleAdmin();
        $editedUser = $userRepository->findOneByRoleUser();

        $client->loginUser($testUser);

        $crawler = $client->request('GET', sprintf('/users/%s/update', $editedUser->getId()));
        $this->assertResponseIsSuccessful();

        $submitButton = $crawler->selectButton("Modifier");
        $testForm = $submitButton->form([
            'user[roles]' => "ROLE_ADMIN",
        ]);
        $client->submit($testForm);

        $this->assertResponseRedirects('/');

    }

    public function testDisplayUserToUpdatePageRedirectsIfInsufficientRole()
    {
        $client = static::createClient();

        $userRepository = $client->getContainer()->get(UserRepository::class);
        $editedUser = $userRepository->findOneBy([]);

        $crawler = $client->request('GET', sprintf('/users/%s/update', $editedUser->getId()));

        $this->assertResponseRedirects('/');
    }
}
