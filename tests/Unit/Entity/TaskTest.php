<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Task;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TaskTest extends KernelTestCase
{
    private $date;

    public function setUp(): void
    {
        $this->date = new \DateTime();
    }

    public function testTaskEntityIsValid(): void
    {
        $user = (new User())
            ->setEmail('toto@test.com')
            ->setUsername('Toto')
            ->setPassword('Todo2022!')
            ->setRoles(["ROLE_USER"])
        ;

        $task = (new Task())
            ->setTitle('Toto')
            ->setContent('Content')
            ->setCreatedAt($this->date)
            ->setAuthor($user)
        ;

        $task->toggle(true);

        $error = self::getContainer()->get('validator')->validate($task);
        $this->assertCount(0, $error);

        $this->assertEquals('Toto', $task->getTitle());
        $this->assertEquals('Content', $task->getContent());
        $this->assertEquals($this->date, $task->getCreatedAt());
        $this->assertEquals(true, $task->isDone());
        $this->assertEquals($user, $task->getAuthor());
    }
}
