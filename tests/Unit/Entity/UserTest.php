<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Task;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserTest extends KernelTestCase
{
    public function testUserEntityIsValid()
    {
        $user = (new User())
            ->setEmail('toto@test.com')
            ->setUsername('Toto')
            ->setPassword('Todo2022!')
            ->setRoles(["ROLE_USER"])
        ;

        $error = self::getContainer()->get('validator')->validate($user);
        $this->assertCount(0, $error);

        $this->assertEquals('toto@test.com', $user->getEmail());
        $this->assertEquals('Toto', $user->getUsername());
        $this->assertEquals('Todo2022!', $user->getPassword());
        $this->assertEquals(["ROLE_USER"], $user->getRoles());

        $task = (new Task())
            ->setTitle('Toto')
            ->setContent('Content')
            ->setCreatedAt(new \DateTime())
        ;

        $this->assertCount(0, $user->getTasks());
        $user->addTask($task);
        $this->assertNotCount(0, $user->getTasks());
        $user->removeTask($task);
        $this->assertCount(0, $user->getTasks());
    }

}
