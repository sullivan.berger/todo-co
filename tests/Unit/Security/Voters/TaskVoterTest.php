<?php

namespace App\Tests\Unit\Security\Voters;

use App\Entity\Task;
use App\Entity\User;
use App\Security\Voters\TaskVoter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Security;

class TaskVoterTest extends KernelTestCase
{
    protected $userToken;
    protected $adminToken;

    protected function setUp(): void
    {
        $user = (new User())
            ->setUsername('Toto')
            ->setEmail('toto@toto.com')
            ->setRoles(['ROLE_USER'])
        ;

        $admin = (new User())
            ->setUsername('Admin')
            ->setEmail('admin@toto.com')
            ->setRoles(['ROLE_ADMIN'])
        ;

        $this->adminToken = $this->createMock(TokenInterface::class);
        $this->userToken = $this->createMock(TokenInterface::class);

        $this->userToken
            ->method('getUser')
            ->willReturn($user)
        ;

        $this->adminToken
            ->method('getUser')
            ->willReturn($admin)
        ;

    }

    private function createTask($user): Task
    {
        return (new Task())
            ->setTitle('Toto')
            ->setContent('Titi')
            ->setAuthor($user)
        ;
    }

    private function provideCases(): \Generator
    {
        $this->setUp();
        yield "Admin can delete anonymous task" => [
            ["delete"],
            $this->createTask(null),
            $this->adminToken,
            VoterInterface::ACCESS_GRANTED
        ];

        yield "User can delete own task" => [
            ["delete"],
            $this->createTask($this->userToken->getUser()),
            $this->userToken,
            VoterInterface::ACCESS_GRANTED
        ];

        yield "User can update own task" => [
            ["update"],
            $this->createTask($this->userToken->getUser()),
            $this->userToken,
            VoterInterface::ACCESS_GRANTED
        ];
    }

    /**
     * @dataProvider provideCases
     */
    public function testVote(
        array $attributes,
        mixed $subject,
        ?TokenInterface $token,
        $expectedVote
    ) {
        $security = $this->getMockBuilder(Security::class)
            ->disableOriginalConstructor()
            ->getMock();

        $user = $token->getUser();
        $returnValue = false;
        if ($user !== null && in_array('ROLE_ADMIN', $user->getRoles(), true)) {
            $returnValue = true;
        }

        $security->method("isGranted")
            ->willReturn($returnValue);

        $voter = new TaskVoter($security);
        $this->assertEquals($expectedVote, $voter->vote($token, $subject, $attributes));
    }
}
